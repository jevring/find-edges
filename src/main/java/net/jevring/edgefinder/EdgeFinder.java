package net.jevring.edgefinder;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.IOException;
import java.util.BitSet;
import javax.imageio.ImageIO;

/**
 * @author markus@jevring.net
 */
public class EdgeFinder {
	public static final int BACK = 0;
	public static final int UP = 1;
	public static final int BACK_UP = 2;
	private final double threshold = 64;

	/**
	 * Finds the edges in an image by checking the change of contrast. If the change of contrast is over 
	 * a certain threshold, the pixel at the edge is potentially part of a line. TO make a line, many 
	 * connected such pixels need to be found.
	 * 
	 * @param file the file that points to the image
	 * @return todo: the edge overlay, or the whole file?
	 * @throws IOException if the file couldn't be found
	 */
	public BufferedImage findEdges(File file) throws IOException {
		long start = System.currentTimeMillis();
		BufferedImage image = ImageIO.read(file);
		System.out.println("Finding edges in " + file.getAbsolutePath());
		System.out.println("image = " + image);
		double[][] luminance = new double[image.getHeight()][image.getWidth()];
		BitSet[][] edgeHints = new BitSet[image.getHeight()][image.getWidth()];
		BufferedImage bufferedImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		for (int h = 0; h < image.getHeight(); h++) {
			for (int w = 0; w < image.getWidth(); w++) {
				int pixel = image.getRGB(w, h);
				int r = pixel & 0x00ff0000 >> 16;
				int g = pixel & 0x0000ff00 >> 8;
				int b = pixel & 0x000000ff >> 0;
				// http://en.wikipedia.org/wiki/Luminance_%28relative%29
				double l = 0.2126 * r + 0.7152 * g + 0.0722 * b;
				luminance[h][w] = l; 
				double backDifference = 0;
				double upDifference = 0;
				double backUpDifference = 0;
				if (w > 0) {
					backDifference = Math.abs(l - luminance[h][w - 1]);
				}
				if (h > 0) {
					upDifference = Math.abs(l - luminance[h - 1][w]);
				}
				if (w > 0 && h > 0) {
					backUpDifference = Math.abs(l - luminance[h - 1][w - 1]);
				}
				BitSet bs = new BitSet(4);
				edgeHints[h][w] = bs;
				if (backDifference > threshold) {
					bs.set(BACK);
				}
				if (upDifference > threshold) {
					bs.set(UP);
				}
				if (backUpDifference > threshold) {
					bs.set(BACK_UP);
				}
			}
		}

		// filter out pixels not connected to anything
		// todo: write all stages to disk?
		for (int h = 0; h < image.getHeight() - 1; h++) {
			for (int w = 0; w < image.getWidth() - 1; w++) {
				BitSet bs = edgeHints[h][w];
				if (!bs.isEmpty()) {
					// todo: this still leaves single pixels. have to fix this
					boolean connectedRight = !edgeHints[h][w + 1].isEmpty();
					boolean connectedDown = !edgeHints[h + 1][w].isEmpty();
					boolean connectedRightDown = !edgeHints[h + 1][w + 1].isEmpty();
					
					if (!connectedRight && !connectedDown && !connectedRightDown) {
						bs.clear();
					}
				}
			}
		}

		for (int h = 0; h < image.getHeight(); h++) {
			for (int w = 0; w < image.getWidth(); w++) {
				BitSet bs = edgeHints[h][w];
				if (!bs.isEmpty()) {
					bufferedImage.setRGB(w, h, Color.BLACK.getRGB());
				} else {
					bufferedImage.setRGB(w, h, Color.WHITE.getRGB());
				}
			}
		}
		
		File output = new File(file.getParentFile(), file.getName().substring(0, file.getName().lastIndexOf('.')) + "_output.png");
		boolean written = ImageIO.write(bufferedImage, "png", output);
		System.out.println("Output: " + output.getAbsolutePath());
		System.out.println("Output image: " + bufferedImage);
		System.out.println("Written: " + written);
		long end = System.currentTimeMillis();
		System.out.println("Time taken: " + (end - start) + " ms");
		return bufferedImage;
	}
}
