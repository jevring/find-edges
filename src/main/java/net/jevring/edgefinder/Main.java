package net.jevring.edgefinder;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * @author markus@jevring.net
 */
public class Main {
	public static void main(String[] args) {
		if (args.length == 1) {

			try {
				EdgeFinder edgeFinder = new EdgeFinder();
				Image edges = edgeFinder.findEdges(new File(args[0]));
				// todo: render this in a swing component.
				// no, start using JavaFX this time. This is a good start.
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			System.err.println("Provide the path of a single file for which to find the edges");
		}
	}
}
